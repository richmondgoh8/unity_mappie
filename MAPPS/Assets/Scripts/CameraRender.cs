﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CameraRender : MonoBehaviour
{

    public RawImage camPreview;
    static WebCamTexture backCam;

    public Text accelerometerText;
    public Text gyroscopeText;
    public Text compassText;
    public Text gpsText;
    public Text accelerationText;
    public Text surfaceText;

    public Text placeholderText;

    public Text totalRecordLabel;
    public Text avgRecordLabel;

    private float xSize = 0.9f * Screen.width;
    private float ySize = 0.2f * Screen.height;
    //private float xPos = 0.2f * Screen.width;
    //private float yPos = 0.0f * Screen.height;

    private Rect GraphRect;

    private int roundValues = 2;

    private bool isCalibrated = false;
    private bool operationStarted = false;

    private float totalCounts = 0;
    private float totalAccX = 0;
    private float totalAccY = 0;
    private float totalAccZ = 0;
    private int decideTrueValue = 0;
    private float time = 0.0f;
    private int accelCount = 0;
    private int minutePassed = -1;
    private float vAcceleration;

    private float nextActionTime = 0.0f;
    private float interval = 1.0f;


    //default is 60hz

    private void DisplayUnityBuiltinSensor()
    {
        float accX = Input.acceleration.x;
        float accY = Input.acceleration.y;
        float accZ = Input.acceleration.z;

        float gyroX = Input.gyro.attitude.eulerAngles.x;
        float gyroY = Input.gyro.attitude.eulerAngles.y;
        float gyroZ = Input.gyro.attitude.eulerAngles.z;

        var compassHead = Input.compass.trueHeading;

        float lat = Input.location.lastData.latitude;
        float longitude = Input.location.lastData.longitude;
        float alt = Input.location.lastData.altitude;
        float hAcc = Input.location.lastData.altitude;
        double timeStamp = Input.location.lastData.timestamp;

        accX = Mathf.Abs(accX);
        accY = Mathf.Abs(accY);
        accZ = Mathf.Abs(accZ);

        float avgAccX;
        float avgAccY;

        if (isCalibrated == false)
        {
            totalCounts += 1;
            totalAccX += accX;
            totalAccY += accY;
            totalAccZ += accZ;
            avgAccX = totalAccX / totalCounts;
            avgAccY = totalAccY / totalCounts;

            if (avgAccX > 0.8f)
            {
                decideTrueValue = 1;
            }
            else if (avgAccY > 0.8f)
            {
                decideTrueValue = 2;
            }
            else
            {
                decideTrueValue = 3;
            }
            return;
        }
        else
        {
            switch (decideTrueValue)
            {
                case 1:
                    vAcceleration = accX;
                    break;
                case 2:
                    vAcceleration = accY;
                    break;
                case 3:
                    vAcceleration = accZ;
                    break;
                default:
                    print("Incorrect intelligence level.");
                    break;
            }
        }



        float totalAcceleration = Mathf.Sqrt(Mathf.Pow(accX, 2) + Mathf.Pow(accY, 2) + Mathf.Pow(accZ, 2));

        if (accelerometerText != null)
        {
            accelCount++;
            totalRecordLabel.text = string.Format("Records: {0}", accelCount.ToString());
            accelerometerText.text = "Acceleration: x=" + System.Math.Round(accX, roundValues) + ", y=" + System.Math.Round(accY, roundValues) + ", z=" + System.Math.Round(accZ, roundValues);
        }


        if (gyroscopeText != null)
            gyroscopeText.text = "Gyro Euler∠ : x=" + System.Math.Round(gyroX, roundValues) + ", y=" + System.Math.Round(gyroY, roundValues) + ", z=" + System.Math.Round(gyroZ, roundValues);

        if (compassText != null)
            compassText.text = "Compass Heading : " + compassHead;

        if (gpsText != null)
            gpsText.text = "Location: " + lat + " " + longitude;
        //gpsText.text = "Location: " + lat + " " + longitude + " Altitude " + alt + " Horizontal Acc " + hAcc + " Time " + timeStamp;

        if (accelerationText != null)
        {

            accelerationText.text = totalAcceleration.ToString();
        }

        // calculate current time in ms
        if (GraphManager.Graph != null)
        {
            //GraphManager.Graph.Plot("Test_WorldSpace", currentDeltaTime, Color.green, new GraphManager.Matrix4x4Wrapper(transform.position, transform.rotation, transform.localScale));
            //Debug.Log(vAcceleration);
            GraphManager.Graph.Plot("Test_ScreenSpace", vAcceleration, Color.green, GraphRect);
            GraphManager.Graph.Plot("Test", totalAcceleration, Color.red, GraphRect);
        }

        float myTimer = Time.time;

        if (myTimer > nextActionTime)
        {
            minutePassed++;
            nextActionTime = myTimer + interval;
            if (minutePassed != 0)
                avgRecordLabel.text = string.Format("{0} Records/s", Mathf.Round(accelCount / minutePassed));

            //Debug.Log("1 second has elpased");
        }





    }

    void SpecialFunc()
    {
        placeholderText.gameObject.SetActive(false);
        isCalibrated = true;
    }

    public void BeginOperation()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        foreach (WebCamDevice cam in devices)
        {
            backCam = new WebCamTexture(1080, 1080, 30);
            camPreview.texture = backCam;
            camPreview.material.mainTexture = backCam;
            backCam.Play();
            var orient = -backCam.videoRotationAngle;
            camPreview.rectTransform.localEulerAngles = new Vector3(0, 0, orient);


            break;
        }

        Input.gyro.enabled = true;

        Input.compass.enabled = true;
        Input.location.Start(); //start gps
        GraphRect = new Rect(50, 680, xSize, ySize);
        operationStarted = true;
        placeholderText.text = "Waiting for Calibration...";
        Invoke("SpecialFunc", 5);
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (operationStarted)
        {
            DisplayUnityBuiltinSensor();
        }
    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("exit");
            SceneManager.LoadSceneAsync("MenuScene");
        }
    }


}
